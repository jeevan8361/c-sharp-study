﻿namespace Maths
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int radius = 4;
            Console.WriteLine("Maths Class::");
            Console.WriteLine("Area of circle using Math.PI,Math.Pow and Rounding with precision => {0}",Math.Round(Math.PI*Math.Pow(radius,2),1));
            Console.WriteLine();
            Console.WriteLine("Square root of 121 => {0} and 47 =>{1}", Math.Sqrt(121), Math.Sqrt(47)); Console.WriteLine();
            Console.WriteLine("Absolute of -78 => {0}", Math.Abs(-78)); Console.WriteLine();
            Console.WriteLine("Floor value of 5.7 => {0} & Ceiling value of 5.7 => {1}",Math.Floor(5.7),Math.Ceiling(5.7));

            
        }
    }
}