﻿using System;
namespace Enum
{
    internal class Program
    {
        enum subjects
        {
            politics,
            history,
            geography=7,
            environment,
            economics
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Printing Enumerations:");
            Console.WriteLine(subjects.politics);
            Console.WriteLine(subjects.history);
            Console.WriteLine(subjects.geography);
            Console.WriteLine(subjects.environment);
            Console.WriteLine(subjects.economics); Console.WriteLine();
            Console.WriteLine("Explicitly converting the types");
            Console.WriteLine("{0}:{1}",subjects.politics,(int)subjects.politics);
            Console.WriteLine("{0}:{1}", subjects.history, (int)subjects.history);
            Console.WriteLine("{0}:{1}", subjects.geography, (int)subjects.geography);
            Console.WriteLine("{0}:{1}", subjects.environment, (int)subjects.environment);
            Console.WriteLine("{0}:{1}", subjects.economics, (int)subjects.economics); Console.WriteLine();
            Console.WriteLine("After changing value");
            Console.WriteLine("{0}:{1}", subjects.politics, (int)subjects.politics);
            Console.WriteLine("{0}:{1}", subjects.history, (int)subjects.history);
            Console.WriteLine("{0}:{1}", subjects.geography, (int)subjects.geography);
            Console.WriteLine("{0}:{1}", subjects.environment, (int)subjects.environment);
            Console.WriteLine("{0}:{1}", subjects.economics, (int)subjects.economics); Console.WriteLine();
            Console.WriteLine("Int to Enumeration conversion");
            Console.WriteLine("value 8=>{0}",(subjects)8);
        }
    }
}