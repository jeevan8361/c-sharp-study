﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace list
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<string> list1 = new List<string>() { "spencer", "jack", "arouville" };
            list1.Add("angelina");
            //foreach(var ele in list1)
            //Console.WriteLine(ele);
            var subs = new List<string>();
            string[] courses = { "cse", "eee", "me", "te"};
            subs.AddRange(courses);
            foreach (var ele in subs)
            Console.WriteLine(ele);
        }
    }
}