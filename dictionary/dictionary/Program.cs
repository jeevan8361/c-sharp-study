﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace dictionary
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string,string> course = new Dictionary<string, string>() { { "science", "engineer" }, { "commerce", "business" }, {"arts","humanities"} };
            course.Add("iti", "industries");//course.Remove("arts");
            course["science"] = "doctor";
            foreach(var ele in course)
               Console.WriteLine("{0}:{1}",ele.Key,ele.Value);
            //Console.WriteLine(course["commerce"]);
            //if(course.ContainsKey("arts"))
                //Console.WriteLine(course["arts"]);

        }
    }
}