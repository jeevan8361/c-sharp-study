﻿using System;
namespace AccessModifiers
{
    internal class Program
    {
        /*class public1
        {
            public int count=10;
            int total=20;
            public void print() { Console.WriteLine("Default Private variable({0}) printed in same class",total); }
            
        }
        static void Main(string[] args)
        {
            public1 obj1=new public1();
            Console.WriteLine("Using public printed in main method:{0}",obj1.count);
            //Console.WriteLine("Without using public:{0}",obj1.total);
            obj1.print();
        }*/
        class protected1
        {
            //public int count = 10;
            protected int total = 20;
            //public void print() { Console.WriteLine("Default Private variable({0}) printed in same class", total); }

        }
        class class2 : protected1
        {
            static void Main(string[] args)
            {
                protected1 obj1 = new protected1();
                class2 obj2 = new class2();
                Console.WriteLine("Accessing protected member in derived from base class:{0}", obj2.total);
                //Console.WriteLine("Without using public:{0}",obj1.total);
                //obj1.print();
            }
        }

    }
}