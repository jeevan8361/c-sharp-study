﻿namespace nullable
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Nullable<int> a = null; //int? a=null; double? a=null; string? a=null;
            //if (a.HasValue)
            Console.WriteLine(a);
            //else Console.WriteLine(a.Value);
            //Console.WriteLine(a.GetValueOrDefault());
            int? i = null;
            //int j = i ?? 10;
            int j = 30; //int j=i??30
            Console.WriteLine(j);
            if (Nullable.Compare<int>(i, j) < 0) Console.WriteLine("i<j");
            else if (Nullable.Compare<int>(i, j) > 0) Console.WriteLine("i>j");
            else Console.WriteLine("i==j");
            bool? isfemale = null;
            if (isfemale == true) Console.WriteLine("Female");
            else if (isfemale == false) Console.WriteLine("Male");
            else Console.WriteLine("No gender chosen");
        }
    }
}