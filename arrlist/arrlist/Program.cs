﻿using System;
using System.Collections;
using System.Collections.Generic;
namespace arrlist
{
    internal class Program
    {
        static void Main(string[] args)
        {
            ArrayList arr1 = new ArrayList();
            ArrayList arr2 = new ArrayList() { 4.98, 16657589484, "CSHARP" };
            arr1.Add(25);
            arr1.Add("Jeevan");
            int[] array1 = new int[] { 85, 46, 87, 23, 56, 97 };
            arr2.AddRange(array1);
            var ele1 = arr2[4];
            int length = arr2.Count;
            //Console.WriteLine(length);
            arr2.Insert(3, "four");
            arr2.InsertRange(4, arr1);
            //arr2.Remove(46);
            //arr2.RemoveAt(2);
            //arr2.RemoveRange(0, 3);
            foreach (var ele in arr2)
                System.Console.WriteLine(ele);
            Console.WriteLine(arr2.Contains(23));
            Console.WriteLine(arr2.Contains(98));
            double total = 0;
            foreach(var ele in arr2)
            {
                if (ele is int)
                {
                    total+=Convert.ToDouble(ele);
                }
                else
                {
                    Console.WriteLine(ele);
                }
            }
            Console.WriteLine(total);
        }
    }
}