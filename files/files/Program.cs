﻿using System.IO;
namespace files
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string contents1 = "offence means crime";
            File.WriteAllText("lawphrase.txt",contents1);
            //string contents2=File.ReadAllText("lawphrase.txt");
            //Console.WriteLine(contents2);
            File.AppendAllText("lawphrase.txt", ",as per indian law");
            string contents2 = File.ReadAllText("lawphrase.txt");
            Console.WriteLine(contents2);
            Console.WriteLine("Checking file exists or not:{0}",File.Exists("lawphrase.txt"));
            //File.Copy("lawphrase.txt", "newphrase1.txt");
            string contents3 = File.ReadAllText("newphrase1.txt");
            Console.WriteLine(contents3);
            //DateTime laccess = File.GetLastAccessTime("lawphrase.txt");
            Console.WriteLine("Last access of lawphrase:{0}",File.GetLastAccessTime("lawphrase.txt"));
            Console.WriteLine("Last write time of lawphrase:{0}", File.GetLastWriteTime("lawphrase.txt"));
        }
    }
}