﻿namespace random1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Random r=new Random();
            Console.WriteLine("Printing any random number: {0}",r.Next());
            Console.WriteLine("Printing any random number below 50: {0}", r.Next(50));
            Console.WriteLine("Printing any random number between 500 and 700: {0}", r.Next(500,700));
        }
    }
}