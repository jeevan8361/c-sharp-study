﻿namespace datetime1
{
    internal class Program
    {
        static void Main(string[] args)
        {
            DateTime d1 = new DateTime();
            Console.WriteLine("DateTime:");
            Console.WriteLine("Default and lowest value:{0}",d1);
            DateTime d2 = new DateTime(2022,07,30,08,02,34,DateTimeKind.Utc);
            Console.WriteLine("Assigned value of date:{0}",d2);
            Console.WriteLine("Today's date:{0}", DateTime.Today);
            Console.WriteLine("Printing current time:{0}",DateTime.Now);
            Console.WriteLine("{0} Hours,{1} Minutes,{2} Seconds",DateTime.Now.Hour,DateTime.Now.Minute,DateTime.Now.Second);
            Console.WriteLine("Tommorrow date and time:{0}",DateTime.Today.AddDays(1));
            Console.WriteLine("Day of the week:{0}",DateTime.Today.DayOfWeek);
            Console.WriteLine("Days in the month of August 2022:{0}", DateTime.DaysInMonth(2022, 8)); Console.WriteLine();
            Console.WriteLine("TimeSpan=>");
            TimeSpan t1 = new TimeSpan(03, 05, 12); //day,hours,minutes
            Console.WriteLine("TimeSpan after two days:{0}",DateTime.Now.Add(t1));
            DateTime d3 = new DateTime(2018, 05, 12);
            TimeSpan t2 = DateTime.Now.Subtract(d3);
            Console.WriteLine("TimeSpan from previous:{0}",t2);

        }
    }
}